/**
 * Acton types for Home.jsx
 */
export const SHOW_HOME = "SHOW_HOME";
export const SHOW_DETAILPART = "SHOW_DETAILPART";
export const ADD_DETAILPRODUCT_IMAGE_URL = "ADD_DETAILPRODUCT_IMAGE_URL";
export const SHOW_CHECKOUT = "SHOW_CHECKOUT";
export const SHOW_PRODUCTLIST = "SHOW_PRODUCTLIST";


/*
 * Action types for Main.jsx 
 */

 export const OPEN_SIGNIN_DIALOG = "OPEN_SIGNIN_DIALOG";
 export const CLOSE_SIGNIN_DIALOG = "CLOSE_SIGNIN_DIALOG";
 export const OPEN_SIGNUP_DIALOG = "OPEN_SIGNUP_DIALOG";
 export const CLOSE_SIGNUP_DIALOG = "CLOSE_SIGNUP_DIALOG";

/*
 *  Action types for FlashDeals.jsx
 */

export const SHOW_FLASHDEAL_PRODUCTS = "SHOW_FLASHDEAL_PRODUCTS";


/* Action types for PRODUCTIMAGEZOOM.jsx */

export const DELET_PRODUCT_IN_BASKET = "DELET_PRODUCT_IN_BASKET";
export const SHOW_DETAILPRODUCT_BY_COLOR = "SHOW_DETAILPRODUCT_BY_COLOR";




/* BASKETS */

export const SHOW_BASKET = "SHOW_BASKET";
export const SHOW_PORDUCTS_IN_BASKET = "SHOW_PORDUCTS_IN_BASKET";
export const Add_PRODUCTINFO_INTO_BASKET = "Add_PRODUCTINFO_INTO_BASKET";
export const INCREMENT_TOTAL_PRICE = "INCREMENT_TOTAL_PRICE";
export const INCREMENT_CURRENT_QUANTITY = "INCREMENT_CURRENT_QUANTITY";
export const DECREMENT_CURRENT_QUANTITY = "DECREMENT_CURRENT_QUANTITY";
export const DECREMENT_TOTAL_PRICE = "DECREMENT_TOTAL_PRICE";


