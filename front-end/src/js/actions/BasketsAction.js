import { 
    SHOW_PORDUCTS_IN_BASKET,
    DELET_PRODUCT_IN_BASKET,
    INCREMENT_TOTAL_PRICE,
    INCREMENT_CURRENT_QUANTITY,
    DECREMENT_CURRENT_QUANTITY,
    DECREMENT_TOTAL_PRICE,
} from "../constants/action-types.js";

function showProductInBasket(){
    return {type:SHOW_PORDUCTS_IN_BASKET}
}
function deleteProductInBasket(payload){
    return {type:DELET_PRODUCT_IN_BASKET,payload}
}

function incrementTotalPrice(payload){
    return {type:INCREMENT_TOTAL_PRICE,payload}
}

function incrementQuantity(payload){
    return {type:INCREMENT_CURRENT_QUANTITY,payload}
}
function decrementQuantity(payload){
    return {type:DECREMENT_CURRENT_QUANTITY,payload}
}


function decrementTotalPrice(payload){
    return {type:DECREMENT_TOTAL_PRICE,payload}
}


export { 
        showProductInBasket,
        deleteProductInBasket,
        incrementTotalPrice,
        incrementQuantity,
        decrementQuantity,
        decrementTotalPrice
    } 