import { 
        SHOW_HOME,
        SHOW_DETAILPART,
        SHOW_BASKET,
        SHOW_CHECKOUT,
        SHOW_PRODUCTLIST,
        ADD_DETAILPRODUCT_IMAGE_URL,
        OPEN_SIGNIN_DIALOG,
        CLOSE_SIGNIN_DIALOG,
        OPEN_SIGNUP_DIALOG,
        CLOSE_SIGNUP_DIALOG,
    } from "../constants/action-types.js";


function showHome(){
    return {type:SHOW_HOME}
};

function showProductList(){
    return {type:SHOW_PRODUCTLIST}
};

function showDetailPart(payload) {
    return { type: SHOW_DETAILPART, payload }
};

function showBaskets(){
    return {type:SHOW_BASKET}
}


function showCheckOut(){
    return {type:SHOW_CHECKOUT}
}


function openSignInDialog(){
    return {type:OPEN_SIGNIN_DIALOG}
}
function closeSignInDialog(){
    return {type:CLOSE_SIGNIN_DIALOG}
}

function openSignUpDialog(){
    return {type:OPEN_SIGNUP_DIALOG}
}

function closeSignUpDialog(){
    return {type:CLOSE_SIGNUP_DIALOG}
}

function addDetailProductImageURL(payload){
    return{ type:ADD_DETAILPRODUCT_IMAGE_URL,payload}
}


export { 
        showDetailPart,
        showHome,
        showBaskets,
        showCheckOut,
        showProductList,
        addDetailProductImageURL,
        openSignInDialog,
        closeSignInDialog,
        openSignUpDialog,
        closeSignUpDialog
    }
