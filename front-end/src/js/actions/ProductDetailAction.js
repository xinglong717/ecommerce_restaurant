import {
    SHOW_DETAILPRODUCT_BY_COLOR,
    Add_PRODUCTINFO_INTO_BASKET
} from "../constants/action-types.js";




function  selectedColorImage(payload){
    return {type:SHOW_DETAILPRODUCT_BY_COLOR,payload}
}
function  addProductInfoInBasket(payload){
    return {type:Add_PRODUCTINFO_INTO_BASKET,payload}
}



export { 
        selectedColorImage,
        addProductInfoInBasket
} 