import { 
        DELET_PRODUCT_IN_BASKET,
        Add_PRODUCTINFO_INTO_BASKET,
        INCREMENT_TOTAL_PRICE,
        INCREMENT_CURRENT_QUANTITY,
        DECREMENT_CURRENT_QUANTITY,
        DECREMENT_TOTAL_PRICE,
        SHOW_PORDUCTS_IN_BASKET
      } from "../constants/action-types.js";
import _ from 'lodash';




const productsStateAddedInBasket = {
    items: [],
    amount_Product_PlacedIn_Cart:0,
    totalPrice:0,
    current_quantity:0,
  };

  function BasketReducer(state = productsStateAddedInBasket, action) {
      
      if (action.type === SHOW_PORDUCTS_IN_BASKET){
        return Object.assign({}, state, {
          items: state.items
        });
      }

      if( action.type === Add_PRODUCTINFO_INTO_BASKET){
        let tempArry = [];
        tempArry.splice(1,0,action.payload);
        return  Object.assign({}, state, {
          items: _.union(state.items,tempArry),
          amount_Product_PlacedIn_Cart:_.union(state.items,tempArry).length
        });
      }
      if( action.type === INCREMENT_TOTAL_PRICE){
        return  Object.assign({}, state, {
          totalPrice:state.totalPrice + action.payload
        });
      }

      if ( action.type === INCREMENT_CURRENT_QUANTITY){
        const mactchRow = state.items.filter(items => items.id === action.payload);
        mactchRow[0].currentQuantity = mactchRow[0].currentQuantity + 1;
        state.current_quantity = mactchRow[0].currentQuantity;
        return  Object.assign({}, state,{items:state.items});
      }

      if ( action.type === DECREMENT_CURRENT_QUANTITY){
        const mactchRow = state.items.filter(items => items.id === action.payload);
        mactchRow[0].currentQuantity = mactchRow[0].currentQuantity - 1;
        state.current_quantity = mactchRow[0].currentQuantity;
        // console.log("FFFFFFFFFF")
        return  Object.assign({}, state,{items:state.items});
      }

      if( action.type === DECREMENT_TOTAL_PRICE){
        return  Object.assign({}, state, {
          totalPrice:state.totalPrice - action.payload
        });
      }

      if (action.type === DELET_PRODUCT_IN_BASKET) {
        const mactchRow = state.items.filter(items => items.id === action.payload);
        mactchRow[0].currentQuantity = 0;
        return Object.assign({}, state, {
          items: state.items.filter(items => items.id !== action.payload),
          amount_Product_PlacedIn_Cart:state.amount_Product_PlacedIn_Cart -1
        }); 
      }
      
      return state;
  };
  export default BasketReducer;