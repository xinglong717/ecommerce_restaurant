import { combineReducers } from 'redux'
import HomeReducer  from './HomeReducer';
import BasketReducer from './BasketReducer';
import FlashDealReducer from './FlashDealsReducer';



const rootReducer = combineReducers({
    Home:HomeReducer,
    FlashDealProducts:FlashDealReducer,
    Baskets:BasketReducer
})

export default  rootReducer;



