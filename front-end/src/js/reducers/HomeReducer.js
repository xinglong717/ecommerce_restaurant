import { 
            SHOW_HOME,
            SHOW_DETAILPART,
            SHOW_CHECKOUT,
            SHOW_PRODUCTLIST,
            SHOW_BASKET,
            OPEN_SIGNIN_DIALOG,
            CLOSE_SIGNIN_DIALOG,
            OPEN_SIGNUP_DIALOG,
            CLOSE_SIGNUP_DIALOG
        } from "../constants/action-types.js";

    const showDepartState = {
        showProductsDetails:false,
        showBaskets:false,
        showCheckOut:false,
        showProductList:false,
        showMainPart:true,
        showBrand:true,
        showProduct:true,
        showService:true,
        showExtraService:true,
        openSignIn:false,
        openSignUp:false,
    };

    function HomeReducer(state = showDepartState, action) {
        
        if(action.type === SHOW_HOME){
            return  Object.assign({}, state, {
                showMainPart:true,
                showBrand:true,
                showProduct:true,
                showService:true,
                showExtraService:true,
                showProductsDetails:false,
                showCheckOut:false,
                showBaskets:false,
                showProductList:false,
            }); 
        }

        if(action.type === SHOW_CHECKOUT){
            return  Object.assign({}, state, {
                showMainPart:false,
                showBrand:false,
                showProduct:false,
                showService:false,
                showExtraService:false,
                showProductsDetails:false,
                showCheckOut:true,
                showBaskets:false,
                showProductList:false,
            }); 
        }

        if(action.type === SHOW_PRODUCTLIST){
            return  Object.assign({}, state, {
                showMainPart:false,
                showBrand:false,
                showProduct:false,
                showService:false,
                showExtraService:false,
                showProductsDetails:false,
                showCheckOut:false,
                showBaskets:false,
                showProductList:true,
            }); 
        }

        if(action.type === SHOW_BASKET) {
            return  Object.assign({}, state, {
                showMainPart:false,
                showBrand:false,
                showProduct:false,
                showService:false,
                showExtraService:false,
                showProductsDetails:false,
                showCheckOut:false,
                showBaskets:true,
                showProductList:false,
            });
        }

        if (action.type === SHOW_DETAILPART) {
            // console.log(action.payload)
            return  Object.assign({}, state, {
                showMainPart:false,
                showBrand:false,
                showProduct:false,
                showService:false,
                showExtraService:false,
                showBaskets:false,
                showCheckOut:false,
                showProductList:false,
                showProductsDetails:action.payload
            });
        }
        if (action.type === OPEN_SIGNIN_DIALOG) {
            return  Object.assign({}, state, {
                openSignIn:true
            }); 
        }
        if (action.type === CLOSE_SIGNIN_DIALOG) {
            return  Object.assign({}, state, {
                openSignIn:false
            });
        }

        if (action.type === OPEN_SIGNUP_DIALOG) {
            // console.log("Opened SingUpDialog!")
            return  Object.assign({}, state, {
                openSignUp:true
            });
        }     
        if (action.type === CLOSE_SIGNUP_DIALOG) {
            return  Object.assign({}, state, {
                openSignUp: false
            });
        }     
        
        return state;
    };

  export default HomeReducer;


