import { 
        ADD_DETAILPRODUCT_IMAGE_URL,
        SHOW_DETAILPRODUCT_BY_COLOR,
        SHOW_FLASHDEAL_PRODUCTS
      } from "../constants/action-types.js";

/* DETAIL PRODUCT IMAGE */

import Deal1 from '../../client/assets/img/products/watch/front-500.jpg';
import Deal2 from '../../client/assets/img/products/watch/back-500.jpg';
import Deal3 from '../../client/assets/img/products/clothes/hoodie_3_front.jpg';
import Deal4 from '../../client/assets/img/products/shirts/T_2_front.jpg';
import Deal5 from '../../client/assets/img/products/shirts/T_7_front.jpg';

import ShirtDetail0002 from '../../client/assets/img/products/shirts/detail/0001/shirt_0001_0001_2.jpg';
import ShirtDetail0003 from '../../client/assets/img/products/shirts/detail/0001/shirt_0001_0001_3.jpg';
import ShirtDetail0004 from '../../client/assets/img/products/shirts/detail/0001/shirt_0001_0001_4.jpg';


const flashDealProducst = {
    dealproducts:[
        { id:1,name:"watch_front",price:45,currentQuantity:0,imgURL:Deal1,bigImg:Deal1,categories:"watch",thumbImage:[{src:Deal1},{src:Deal2}],colorImage:[{src:Deal1},{src:Deal2}]},
        { id:2,name:"watch_back",price:12,currentQuantity:0,imgURL:Deal2,bigImg:Deal2,categories:"watch",thumbImage:[{src:Deal1},{src:Deal2}],colorImage:[{src:Deal1},{src:Deal2}]},
        { id:3,name:"T-shirt1",price:18,currentQuantity:0,imgURL:Deal3,bigImg:Deal3,categories:"shirts",thumbImage:[{src:ShirtDetail0002},{src:ShirtDetail0003},{src:ShirtDetail0004}],colorImage:[{src:ShirtDetail0002},{src:ShirtDetail0003}]},
        { id:4,name:"T-shirt2",price:32,currentQuantity:0,imgURL:Deal4,bigImg:Deal4,categories:"shirts",thumbImage:[{src:ShirtDetail0003},{src:ShirtDetail0002},{src:ShirtDetail0004}],colorImage:[{src:ShirtDetail0003},{src:ShirtDetail0004},{src:ShirtDetail0002}]},
        { id:5,name:"T-shirt3",price:21,currentQuantity:0,imgURL:Deal5,bigImg:Deal5,categories:"shirts",thumbImage:[{src:ShirtDetail0004},{src:ShirtDetail0003},{src:ShirtDetail0002}],colorImage:[{src:ShirtDetail0004},{src:ShirtDetail0003},{src:ShirtDetail0002}]}
    ],
    currentImageURL:"",
    selectedProduct:[]
  };
  
  function FlashDealReducer(state = flashDealProducst, action) {
    
    if ( action.type === ADD_DETAILPRODUCT_IMAGE_URL){
      const mactchRow = state.dealproducts.filter(dealproducts => dealproducts.id === action.payload);
      return  Object.assign({}, state,{selectedProduct:mactchRow},
                                      {currentImageURL:mactchRow[0].imgURL});
    }
   

    if (action.type === SHOW_DETAILPRODUCT_BY_COLOR){ 
        return Object.assign({}, state, {
          currentImageURL: action.payload
        });
      }
    if (action.type === SHOW_FLASHDEAL_PRODUCTS){ 
        return Object.assign({}, state, {
          dealproducts: state.dealproducts
        });
      }
      
      return state;
  };
  export default FlashDealReducer;