import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';



import { makeStyles } from '@material-ui/core/styles';
/* DETAIL PRODUCT IMAGE */
import Deal1 from '../assets/img/products/watch/u1.jpg';
import Deal2 from '../assets/img/products/watch/u2.jpg';
import Deal3 from '../assets/img/products/watch/u3.jpg';
import Deal4 from '../assets/img/products/watch/u4.jpg';




const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    avatar: {
      margin: 10,
      width: 50,
      height: 50,
    },
    button: {
      margin: theme.spacing(1),
    },
    card: {
      transition: "0.3s",
      boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
      "&:hover": {
        boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
      },
      maxWidth: 345,
    },
    couponbg:{
      backgroundColor: `#ff0000`,
      // margin:theme.spacing(2),
    },
    footer: {
      backgroundColor: theme.palette.background.paper,
      marginTop: theme.spacing(8),
      padding: theme.spacing(6, 0),
    },
    detailpart: {
      backgroundColor: theme.palette.background.paper,
    },
    icon: {
      margin: theme.spacing(1),
      fontSize: 32,
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: 200,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    media: {
      height: theme.spacing(12),
    },
    dealmedia:{
      // padding:theme.spacing(1),
      height: theme.spacing(32),
    },
    paper:{
      marginTop:theme.spacing(1),
    },
    couponpaper:{
      margin:theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    toolbar: {
      marginTop :theme.spacing(8), 
      borderBottom: `1px solid ${theme.palette.divider}`,
      backgroundColor:`#ffffff`,
    },
    toolbarTitle: {
      flex: 1,
      fontWeight: `bold`,
      marginTop :theme.spacing(4),
    },
    brandTitle:{
      flex: 1,
      marginTop :theme.spacing(0),
    },
    toolbarSecondary: {
      justifyContent: 'space-between',
      overflowX: 'auto',
    },
    toolbarLink: {
      padding: theme.spacing(1),
      flexShrink: 0,
    }
  }));

export default function Service(){
    const classes = useStyles();
    return (
         <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography
                    component="h2"
                    variant="h5"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.toolbarTitle}
                  >
                  JUST FOR YOU
                </Typography>
                <Typography variant="subtitle1" color="textSecondary">
                Hot items.Affordable prices
              </Typography>
            </Grid>
            <Grid item xs>
              
                <Card className={classes.card}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.dealmedia}
                      image={Deal1}
                      title=" "
                    />
                  </CardActionArea>
                  <CardContent>
                  <Typography variant="body2" color="textSecondary" component="p">$45</Typography>
                  </CardContent>
                </Card>
                
            </Grid>
            <Grid item xs>
                <Card className={classes.card}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.dealmedia}
                      image={Deal2}
                      title=" "
                    />
                  </CardActionArea>
                  <CardContent>
                  <Typography variant="body2" color="textSecondary" component="p">$45</Typography>
                  </CardContent>
                </Card>
            </Grid>
            <Grid item xs>
                <Card className={classes.card}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.dealmedia}
                      image={Deal3}
                      title=" "
                    />
                  </CardActionArea>
                  <CardContent>
                  <Typography variant="body2" color="textSecondary" component="p">$45</Typography>
                  </CardContent>
                </Card>
            </Grid>
            <Grid item xs>
                <Card className={classes.card}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.dealmedia}
                      image={Deal4}
                      title=" "
                    />
                  </CardActionArea>
                  <CardContent>
                  <Typography variant="body2" color="textSecondary" component="p">$45</Typography>
                  </CardContent>
                </Card>
            </Grid>
            <Grid item xs>
                <Card className={classes.card}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.dealmedia}
                      image={Deal4}
                      title=" "
                    />
                  </CardActionArea>
                  <CardContent>
                  <Typography variant="body2" color="textSecondary" component="p">$45</Typography>
                  </CardContent>
                </Card>
            </Grid>
          </Grid>
        </Container>
    );
}