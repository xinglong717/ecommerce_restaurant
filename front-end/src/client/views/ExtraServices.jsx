import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import { InputGroup,Button,FormControl} from "react-bootstrap";


import image1 from '../assets/img/extraservice/1.png';
import image2 from '../assets/img/extraservice/2.png';
import image3 from '../assets/img/extraservice/3.png';
import image4 from '../assets/img/extraservice/4.png';
import image5 from '../assets/img/extraservice/5.png';
import image6 from '../assets/img/extraservice/6.png';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom:theme.spacing(4),
  },
  card: {
    maxWidth: 345,
  },
  images:{
    marginTop:theme.spacing(8),
  },
  brandcard:{
    maxHeight: theme.spacing(24),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0),
  },
  detailpart: {
    marginTop:theme.spacing(4),
    backgroundColor: theme.palette.background.paper,
  },
  productsList:{
    marginTop:theme.spacing(1),
  },
  dealmedia:{
    height: theme.spacing(32),
  },
  paper:{
    marginTop:theme.spacing(1),
  },
  service:{
    marginTop:theme.spacing(4),
  },
  subscription:{
    fontSize:12
  },
  subscriptionInput:{
    fontSize:12,
    maxWidth:theme.spacing(28),
  },
  toolbarTitle: {
    flex: 1,
    fontWeight:`bold`,
    // margingTop:theme.spacing(4),
    // marginBottom:theme.spacing(1),
  }
}));



export default  function ExtraService(){
  const classes = useStyles();
    return(
     
        <footer className={classes.detailpart}>
        <CssBaseline />
        <Divider/>
        <Container maxWidth="lg">
          <Grid container spacing={1} alignContent = "center">
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                color="inherit"
                style={{ borderRight: '0.1em solid silver', padding: '0.5em' }}
              >
                <Grid item>
                  <img src = {image1} className = {classes.images} alt ="" />
                </Grid>
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        className={classes.toolbarTitle}
                      >
                      Great Value
                    </Typography>
                    <Typography
                    component="h2"
                    variant="subtitle2"
                    color="textSecondary"
                    align="center"
                    >
                      We offer competitive prices
                      on our 100million plus
                      product range.
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{ borderRight: '0.1em solid silver', padding: '0.5em' }}
              >
                <Grid item>
                  <img src = {image2} className = {classes.images} alt ="" />
                </Grid>
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="left"
                        noWrap
                        className={classes.toolbarTitle}
                      >
                      Worldwide Delivery
                    </Typography>
                    <Typography
                    component="h2"
                    variant="subtitle2"
                    color="textSecondary"
                    align="center"
                    >
                     With sites in 5 languages,We
                     ship to over 200 countries &
                     regions.
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{ borderRight: '0.1em solid silver', padding: '0.5em' }}
              >
                <Grid item>
                  <img src = {image3} className = {classes.images} alt ="" />
                </Grid>
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        noWrap
                        className={classes.toolbarTitle}
                      >
                      Safe Payment
                    </Typography>
                    <Typography
                    component="h2"
                    variant="subtitle2"
                    color="textSecondary"
                    align="center"
                    >
                      Pay with the world's most
                      popular and secure Payment
                      methods.
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{ borderRight: '0.1em solid silver', padding: '0.5em' }}
              >
                <Grid item>
                  <img src = {image4} className = {classes.images} alt ="" />
                </Grid>
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        noWrap
                        className={classes.toolbarTitle}
                      >
                      Shop with Confidence
                    </Typography>
                    <Typography
                    component="h2"
                    variant="subtitle2"
                    color="textSecondary"
                    align="center"
                    >
                      Our Buyer Protection covers your 
                      purchase from click to 
                      deliver.
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                style={{ borderRight: '0.1em solid silver', padding: '0.5em' }}
              >
                <Grid item>
                  <img src = {image5} className = {classes.images} alt ="" />
                </Grid>
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        noWrap
                        className={classes.toolbarTitle}
                      >
                      24/7 Help Center
                    </Typography>
                    <Typography
                    component="h2"
                    variant="subtitle2"
                    color="textSecondary"
                    align="center"
                    >
                      Round-the-clock assitance
                      for a smooth  shopping 
                      experience.
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  <img src = {image6} className = {classes.images} alt ="" />
                </Grid>
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        noWrap
                        className={classes.toolbarTitle}
                      >
                      Shop On-The-Go
                    </Typography>
                    <Typography
                    component="h2"
                    variant="subtitle2"
                    color="textSecondary"
                    align="center"
                    >
                      Download the app and
                      get the world of AliExpress at
                      your fingertips.
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
        <Divider/>
        <Container className = {classes.service}>
          <Grid container spacing={1} alignContent = "center">
            <Grid item xs = {12} md = {4}>
              <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="left"
                        className={classes.toolbarTitle}
                      >
                      Subscription
              </Typography>
                <InputGroup >
                  <FormControl
                    placeholder="Please enter your email"
                    aria-label="Recipient's username"
                    aria-describedby="basic-addon2"
                    className = {classes.subscriptionInput}
                    />
                  <InputGroup.Append>
                    <Button variant="danger" className = {classes.subscription}>Subscription</Button>
                  </InputGroup.Append>
                </InputGroup>
                <Typography
                        component="subtitle2"
                        variant="subtitle2"
                        color="textSecondary"
                        align="left"
                        // className={classes.toolbarTitle}
                      >
                      Register now to get updates on promotions and coupons.
              </Typography>
              <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="left"
                        className={classes.toolbarTitle}
                      >
                      Stay Connected
              </Typography>
            </Grid> 
            <Grid item xs = {12} md = {2}>
              <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        className={classes.toolbarTitle}
                      >
                      How to Buy
              </Typography>
              <Typography
                        component="subtitle2"
                        variant="subtitle2"
                        color="textSecondary"
                        align="center"
                      >
                      Making Payments
                      Delivery Options
                      Buyer Protection
              </Typography>
            </Grid> 
            <Grid item xs = {12} md = {4}>
              <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        className={classes.toolbarTitle}
                      >
                     Customer Service
              </Typography>
              <Typography
                        component="subtitle2"
                        variant="subtitle2"
                        color="textSecondary"
                        align="center"
                      >
                      Customer Service
                      Transaction Service Agreement
                      Take Our Survey
              </Typography>
            </Grid> 
            <Grid item xs = {12} md = {2}>
            <Typography
                        component="h2"
                        variant="subtitle1"
                        color="inherit"
                        align="center"
                        className={classes.toolbarTitle}
                      >
                     Partner Promotion
              </Typography>
              <Typography
                        component="subtitle2"
                        variant="subtitle2"
                        color="textSecondary"
                        align="center"
                      >
                      Partnerships
                      Affilliate Program
              </Typography>
            </Grid> 
          </Grid>
        </Container>
      </footer>
    );
}