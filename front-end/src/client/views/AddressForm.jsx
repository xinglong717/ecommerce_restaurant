import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';    
import TableRow from '@material-ui/core/TableRow';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { Form,Button,Row,Col} from "react-bootstrap";
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';



import image1 from '../assets/img/payment/pay-american-ex.png';
import image2 from '../assets/img/payment/pay-bank.png';
import image3 from '../assets/img/payment/pay-mastercard.png';
import image4 from '../assets/img/payment/pay-visa.png';
import image5 from '../assets/img/payment/pay-visa-el.png';
import Basket from '../components/Basket';




const useStyles = theme =>({
  images:{
    padding:theme.spacing(1),
  },
  signInLink:{
    fontWeight:`bold`,
  },
  main:{
    marginTop:theme.spacing(4),
    marginLeft:theme.spacing(10)
  }
})

const mapStateToProps = state => {
  // console.log("Current Quantity:"+state.Baskets.current_quantity)
  return { 
      items: state.Baskets.items,
      currentQuantity:state.Baskets.current_quantity,
      totalPrice:state.Baskets.totalPrice,
  };
};



class ConnectedAddressForm extends React.Component{
  render(){
    const {classes} = this.props;
    return(
      <React.Fragment>
        <Typography variant="subtitle1" gutterBottom>
          You are checking out as a guest,Have an account? 
          <Link  
                component="button"
                variant="subtitle1"
                className={classes.signInLink}
                onClick={this.onClickViewMore} >
                SingIn
              </Link> Or you can create an account during checkout
        </Typography>
        <Typography variant="h6" gutterBottom>
          1.Please fill in your shipping information:
        </Typography>
        <Form className = {classes.main}>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Email address
            </Form.Label>
            <Col sm={7}>
              <Form.Control type="email" placeholder="We'll send an order confirmation to this address" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Contact Name:
            </Form.Label>
            <Col sm={7}>
              <Form.Control type="email" placeholder="Your Name" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formGridState">
            <Form.Label column sm={2}>Country/Region:</Form.Label>
            <Col sm={7}>
              <Form.Control as="select">
                <option>Choose...</option>
                <option>United State</option>
              </Form.Control>
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Street Address:
            </Form.Label>
            <Col sm={7}>
              <Form.Control type="email" placeholder="Street Address" />
              <br></br>
              <Form.Control type="email" placeholder="Apartment,suit,unit,etc(optional)" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formGridState">
            <Form.Label column sm={2}>State/Province/Region:</Form.Label>
            <Col sm={7}>
              <Form.Control as="select">
                <option>Choose...</option>
                <option>United State</option>
              </Form.Control>
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              City/Department:
            </Form.Label>
            <Col sm={7}>
              <Form.Control type="email" placeholder="City/Department" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Zip/Postal Code:
            </Form.Label>
            <Col sm={7}>
              <Form.Control type="email" placeholder="Zip/Postal Code" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalCheck">
            <Col sm={{ span: 10, offset: 2 }}>
              <Form.Check label="My Address doesnot have a ZIP code" />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column sm={2}>
              Mobile:
            </Form.Label>
            <Col sm={2}>
              <Form.Control type="email" placeholder="+1" />
            </Col>
            <Col sm={5}>
              <Form.Control type="email" placeholder="" />
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Col sm={{ span: 10, offset: 2 }}>
              <Button type="submit" variant="warning">Save and Ship this address</Button>
            </Col>
          </Form.Group>
          <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Label>Example textarea</Form.Label>
            <Form.Control as="textarea" rows="3" />
          </Form.Group>
        </Form>
        <Typography variant="h6" gutterBottom>
          2.Review and Confirm your order(1items):
        </Typography>
        <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell align="center">ProductName</TableCell>
                    <TableCell align="center">price</TableCell>
                    <TableCell align="center">Quantity</TableCell>
                    <TableCell align="right">total</TableCell>
                    <TableCell align="center">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.props.items.map((item,i) => (  
                   <Basket 
                            key = {i}
                            _id = {i}
                            currentQuantity = {this.props.currentQuantity}
                            item = {item}
                    />
                  ))}
                  <TableRow>
                    <TableCell colSpan = {5}>
                      <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Leave a message for this seller:</Form.Label>
                        <Form.Control as="textarea" rows="2" />
                      </Form.Group>
                        <Typography
                          component="h2"
                          variant="subtitle2"
                          color="textSecondary"
                          align="left"
                        >
                        Max.1,000 English characters or Arabic numbers only.No Html codes.
                      </Typography>
                    </TableCell>
                  </TableRow>
                  <TableRow>
                  <TableCell rowSpan={3} />
                    <TableCell colSpan={3}>Total:</TableCell>
                    {/* Total Price */}
                    <TableCell align="right">{this.props.totalPrice}</TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              {/* <CssBaseline /> */}
          <Grid container
                direction="column"
                justify="center"
                alignItems="flex-end"
                color="inherit">
          <Grid item xs= {12} md = {7}>
                
          </Grid>
          <Grid item xs= {12} md = {5}>
                   
          </Grid>
        </Grid>
        <Divider/>
        <Container maxWidth="lg">
          <Grid container spacing={1} direction="row"
                  justify="center"
                  alignItems="center">
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                color="inherit"
              >
                <Grid item>
                  <Typography
                        component="h2"
                        variant="subtitle1"
                        color="textSecondary"
                        align="left"
                        noWrap
                      >
                      Payment Methods:
                    </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                color="inherit"
              >
                <Grid item>
                  <img src = {image1} className = {classes.images} alt ="" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  <img src = {image2} className = {classes.images} alt ="" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  <img src = {image3} className = {classes.images} alt ="" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  <img src = {image4} className = {classes.images} alt ="" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm = {2}>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >
                <Grid item>
                  <img src = {image5} className = {classes.images} alt ="" />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Container>
        <Divider/>
    </React.Fragment>
    );
  }
}


ConnectedAddressForm.propTypes = {
  classes: PropTypes.object.isRequired
};
const AddressForm = connect(mapStateToProps,null)(ConnectedAddressForm);

export default withStyles(useStyles)(AddressForm);