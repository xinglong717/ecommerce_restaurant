import React from 'react';
import { Nav,NavDropdown,Navbar} from "react-bootstrap";
import { Container } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ProductImageZoom from '../components/ProductImageZoom';


const useStyles = theme => ({
    root: {
        flexGrow: 1,
    },
    toptitle:{
        marginTop:theme.spacing(4),
        marginBottom:theme.spacing(2),
        textAlign:'center'
    }
});





class ProductDetails extends React.Component{
    render(){
        const {classes} = this.props;
        return( 
                <Container >
                    <h3 className = {classes.toptitle}>
                        PUMASS Official Store
                    </h3>
                    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                        <Navbar.Brand href="#home">Store Home Page</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Product" id="collasible-nav-dropdown">
                                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link href="#features">Features</Nav.Link>
                                <Nav.Link href="#pricing">Pricing</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                    <ProductImageZoom/>
                </Container>
                   
        );
    }
}

export default withStyles(useStyles)(ProductDetails);