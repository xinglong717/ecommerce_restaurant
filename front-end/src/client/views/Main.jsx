import React from 'react';
import {connect} from 'react-redux';    
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';

import Laptop from '@material-ui/icons/Laptop';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import PhonePortrait from '@material-ui/icons/StayCurrentPortrait';
import ShoppingBasket from '@material-ui/icons/ShoppingBasketOutlined';
import Watch from '@material-ui/icons/WatchOutlined';
import Weekend from '@material-ui/icons/WeekendOutlined';
import DirectionsRun from '@material-ui/icons/DirectionsRunOutlined';
import Accessibility from '@material-ui/icons/AccessibilityNewOutlined';




import Avatar from '@material-ui/core/Avatar';

/* Image Carousel */
import { Carousel } from 'react-responsive-carousel';
import Carousel1 from '../assets/img/slide1.png';
import Carousel2 from '../assets/img/slide2.png';
import Carousel3 from '../assets/img/slide3.png';
import Carousel4 from '../assets/img/slide4.png';
import Carousel5 from '../assets/img/slide5.png';
import Carousel6 from '../assets/img/slide6.png';
import Carousel7 from '../assets/img/slide7.png';
import Carousel8 from '../assets/img/slide8.png';

/* Image Product */
import Product1 from '../assets/img/products/shoes/women/HTB1I4MFllDH8KJjSspnq6zNAVXaz.jpg_350x350.jpg';
import Product2 from '../assets/img/products/phones/phone1.jpg';
import Product3 from '../assets/img/products/diamond/2.png';
import Product4 from '../assets/img/products/socks/women1.jpg';
import Product5 from '../assets/img/products/shoes/women1.png';
import Product6 from '../assets/img/products/jakets/1.png';


/* Account Avater */
import Account from '../assets/img/account.jpg';
import Coupon from '../assets/img/coupon.png';
import Service1 from '../assets/img/service1.png';
import Service2 from '../assets/img/service2.png';
import Money from '../assets/img/money.png';
import ServiceMark from '../assets/img/serviceMark.png';

import Typography from '@material-ui/core/Typography';


import Dialog from '@material-ui/core/Dialog';
import SignIn from '../components/SignIn';
import SignUp from '../components/SignUp';
import RepProduct from '../components/RepProduct';

import {openSignInDialog} from '../../js/actions/Home';
import {closeSignInDialog} from '../../js/actions/Home';
import {openSignUpDialog} from '../../js/actions/Home';
import {closeSignUpDialog} from '../../js/actions/Home';


const useStyles = theme => ({
    root: {
      flexGrow: 1,
      marginTop :theme.spacing(2),
      backgroundColor:`#ffffff`,
    },
    avatar: {
      margin: theme.spacing(1),
      width: theme.spacing(5),
      height:theme.spacing(5),
    },
    button: {
     marginBottom:theme.spacing(1),
     margin:theme.spacing(1),
    },
    userzone:{
        borderRadius: theme.spacing(2),
        borderColor:`#ff0000`,
        fontFamily:`Arial, sans-serif`,
        fontWeight:`bold`,
        background: `#ffffff`,
        color:`#ff0000`,
        width:theme.spacing(18),
        marginTop: theme.spacing(-1),
    },
    couponpaper:{
        marginTop:theme.spacing(2),
        width:theme.spacing(28),
        height: theme.spacing(28),
        backgroundColor: `#ff0000`,
    },
    coupon:{
        width:theme.spacing(26),
        height: theme.spacing(7),
    },
    customeService:{
        marginTop:theme.spacing(1),
        width:theme.spacing(26),
        height:theme.spacing(17),
    },
    customserviceImg:{
        width:theme.spacing(8),
        height:theme.spacing(8),
    },
    useZoneText:{
        marginTop:theme.spacing(1),
        fontSize: 12
    },
    lists:{
        height:theme.spacing(6),
        // fontSize:12
    },
    accounttitle:{
        fontFamily:`Arial, sans-serif`,
        fontWeight:`bold`,
    }
  });

  function mapDispatchToProps(dispatch) {
    return{
      openSignInDialog: () => { dispatch(openSignInDialog()) },
      closeSignInDialog:() =>{ dispatch(closeSignInDialog())},
      openSignUpDialog:()=>{dispatch(openSignUpDialog())},
      closeSignUpDialog:()=>{dispatch(closeSignUpDialog())}
    };
  }

  const mapStateToProps = state => {
    return { 
            openSignIn:state.Home.openSignIn,
            openSignUp:state.Home.openSignUp
          };
  };


class ConnectedMain extends React.Component{
   constructor(props){
       super(props);
       this.state = {
        leftListCategorys:[ 
                            {
                                iText:"Men's fashion",
                                iIcon:<ShoppingBasket/>,
                                id:"Men's Fashion",
                            },
                            {
                                iText:"Women's fashion",
                                iIcon:<ShoppingBasket/>,
                                id:"Woman fashion",
                            },
                            {
                                iText:"Telephony",
                                iIcon:<PhonePortrait/>,
                                id:"Telephony",
                            },
                            {
                                iText: "Computing",
                                iIcon:<Laptop/>,
                                id: "Computing",
                            },
                            {
                                iText: "electronics",
                                iIcon:<PhotoCamera/>,
                                id: "electronics",
                            },
                            {
                                iText: "Jewelry and Watches",
                                iIcon:<Watch/>,
                                id: "Jewelry and Watches",
                            },
                            {
                                iText: "House and Garden",
                                iIcon:<Weekend/>,
                                id: "House and Garden",
                            },  
                            {
                                iText: "Bags and Footwear",
                                iIcon:<ShoppingBasket/>,
                                id: "Bags and Footwear",
                            },  
                            {
                                iText:"Sports and Leisure",
                                iIcon:<DirectionsRun/>,
                                id: "Sports and Leisure",
                            },  
                            {
                                iText:"Health and Beauty",
                                iIcon:<Accessibility/>,
                                id: "Health and Beauty",
                            } 
                        ],
        
        CarouselCategorys:[
            {
                id:1,
                imgURL:Carousel1
            },
            {
                id:2,
                imgURL:Carousel2
            },
            {
                id:3,
                imgURL:Carousel3
            },
            {
                id:4,
                imgURL:Carousel4
            },
            {
                id:5,
                imgURL:Carousel5
            },
            {
                id:6,
                imgURL:Carousel6
            },
            {
                id:7,
                imgURL:Carousel7
            }
        ],
        FlashDealsCategorys:[
            {imgURL:Product1},
            {imgURL:Product2},
            {imgURL:Product3},
            {imgURL:Product4},
            {imgURL:Product5},
            {imgURL:Product6}
        ],
    };
   }
    
    handleSignInClickOpen = () => {
        this.props.openSignInDialog();
    }
    
    signInHandleClose = () => {
        this.props.closeSignInDialog();
    }

    handleSignUpClickOpen = () => {
        this.props.openSignUpDialog();
    }

    signUpHandleClose = () => {
        this.props.closeSignUpDialog();
    }

    test = () =>{
        console.log("FFF")
    }

    render(){
    const {classes} = this.props;
    
    return(
        <Grid container className = {classes.root}>
            <Grid item xs={12} md = {3}>
                    <List component="nav" aria-label="Main mailbox folders">
                        {
                            this.state.leftListCategorys.map((category)=>{
                              return(
                                <ListItem button key = {category.id} className = {classes.lists} onMouseOver = {this.test}>
                                    <ListItemIcon >
                                        {category.iIcon}
                                    </ListItemIcon> 
                                    <ListItemText primary={category.iText}/>
                                </ListItem>
                              );
                            })
                          }
                    </List>
            </Grid> 

        {/* START IMAGE CAOUSERL */}
    
            <Grid item xs={12} md = {6}>
                <Grid container direction="column" justify="center" alignItems="center" >
                    <Carousel autoPlay = {true} showArrows = {false} showThumbs ={false} showStatus = {false} infiniteLoop = {true}>
                        <div><img src={Carousel1} alt ="Breakfast"/></div>
                        <div><img src={Carousel2} alt =""/></div>
                        <div><img src={Carousel3} alt =""/></div>
                        <div><img src={Carousel4} alt =""/></div>
                        <div><img src={Carousel5} alt =""/></div>
                        <div><img src={Carousel6} alt =""/></div>
                        <div><img src={Carousel7} alt =""/></div>
                        <div><img src={Carousel8} alt =""/></div>
                    </Carousel>
        
                <Grid container>
                {
                            this.state.FlashDealsCategorys.map((category,_id)=>{
                              return(
                                <RepProduct
                                    key={_id}
                                    imgURL={category.imgURL}
                                />
                              );
                            })
                }
                </Grid>
            </Grid>         
        </Grid>
        {/* End Image Carousel */}

        {/* BEAGIN ACCOUNT */}
        <Grid item xm={12} md = {3}>
        {/* square ={true}   */}
            <Paper elevation={0}   > 
                <Grid container direction="column" justify="center" alignItems="stretch" >
                    <Grid item align = "center">
                        <Avatar alt="Remy Sharp" src={Account} className={classes.avatar}/>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="subtitle2" component="h2" align = "center" className = {classes.accounttitle}>
                            {localStorage.getItem('userInfo')}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Container>
                            <div align = "center">
                                <Button variant="contained" color="secondary" className={classes.button} onClick = {this.handleSignInClickOpen}> Sign In </Button>
                                <Dialog open={this.props.openSignIn} onClose={this.signInHandleClose} aria-labelledby="form-dialog-title">
                                    <SignIn/>
                                </Dialog>
                                <Button variant="contained" color="default" className={classes.button} onClick = {this.handleSignUpClickOpen}> Sign Up </Button> 
                                <Dialog open={this.props.openSignUp} onClose={this.signUpHandleClose} aria-labelledby="form-dialog-title">
                                    <SignUp/>
                                </Dialog>
                            </div>
                        </Container>
                    </Grid>
                    <Grid item>                        
                        <Grid container direction="column" justify="center" alignItems="center" >
                            <Paper className = {classes.couponpaper} align = "center" elevation = {0}>
                                <Grid item>
                                    <Typography  gutterBottom  className = {classes.userzone} variant="subtitle2" component="h6" align = "center">
                                        NEW USER ZONE
                                    </Typography>
                                    <img src = {Coupon} alt =""  className = {classes.coupon}/>
                                    <Paper className = {classes.customeService} elevation = {0}>
                                    <Grid container>
                                        <Grid item xs={12}>
                                            <Typography gutterBottom className = {classes.useZoneText} color = 'textSecondary'>
                                                <img src = {Money}alt ="" />Refund Guarantee
                                            </Typography>  
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography gutterBottom className = {classes.useZoneText} color = 'textSecondary'>
                                                <img src = {ServiceMark} alt ="" />24/7 Customer Service
                                              </Typography>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Paper elevation = {0} className = {classes.customservice}><img src = {Service1} className = {classes.customserviceImg} alt ="" /></Paper>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Paper elevation = {0} className = {classes.customservice}><img src = {Service2} className = {classes.customserviceImg} alt ="" /></Paper>
                                        </Grid>
                                    </Grid>
                                    </Paper>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>                                          
            </Paper>
        </Grid>
        {/* END ACCOUNT */}
        </Grid>
    );}
}

ConnectedMain.propTypes = {
    classes: PropTypes.object.isRequired
};
const Main = connect(mapStateToProps,mapDispatchToProps)(ConnectedMain);
export default withStyles(useStyles)(Main);
