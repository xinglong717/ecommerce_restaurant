import React from 'react';
import { connect } from "react-redux";
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';  
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Deal from '../components/Deal';

import { withStyles } from '@material-ui/core/styles';
import {showProductList} from '../../js/actions/Home';


/* BARANDS IMAGE */
import barnd1 from '../assets/img/brands/1.jpg';
import barnd2 from '../assets/img/brands/2.jpg';
import barnd3 from '../assets/img/brands/3.jpg';


const useStyles = theme => ({
  root: {
    flexGrow: 1,
  },
  card: {
    maxWidth: theme.spacing(28),
  },
  cardAction: {
    display: 'block',
    width:theme.spacing(28),
    textAlign: 'center'
  },
  brandcard:{
    maxHeight: theme.spacing(24),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0),
  },
  detailpart: {
    backgroundColor: theme.palette.background.paper,
  },
  productsList:{
    marginTop:theme.spacing(1),
  },
  dealmedia:{
    height: theme.spacing(24),
  },
  paper:{
    marginTop:theme.spacing(1),
  },
  flashdeals: {
    fontWeight:`bold`,
  },
  featuredbrands: {
    marginTop:theme.spacing(8),
    fontWeight:`bold`,
  },
  flashdealstitle:{
    marginLeft:theme.spacing(2),
  }
});


const mapStateToProps = state => {
  return { 
      flashDealProducts: state.FlashDealProducts.dealproducts,
  };
};

  function mapDispatchToProps(dispatch) {
    return{
      gotoProductList: () => { dispatch(showProductList()) },
    };
  }

class ConnectedBrands extends React.Component{
  
  onClickViewMore =()=>{
    console.log("FFFF")
    this.props.gotoProductList();
  }
  render(){
    const {classes} = this.props;
    return(
      <footer className={classes.detailpart}>
        <br></br>
        <br></br>
      <Container maxWidth="lg" className = {classes.root}>
        <Grid container spacing={1} >
          <Grid item xs={12}>
            <Typography
                  component="h2"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.flashdeals}
                >
                Flash Deals
              </Typography>
          </Grid>
          <Grid item xs={12} sm = {3}>
            <Typography
                  component="h2"
                  variant="subtitle1"
                  color="inherit"
                  align="left"
                  noWrap
                >
                Hot items.Affordable prices
              </Typography>
          </Grid>
          <Grid item xs={12} sm={9}>
            <Typography
              component="h2"
              variant="subtitle2"
              color="primary"
              align="right"
              noWrap
            >
              <Link  
                component="button"
                variant="body2"
                className={classes.flashdeals}
                onClick={this.onClickViewMore} >
                View More
              </Link>
            </Typography>
          </Grid>
            {
              this.props.flashDealProducts.map((_deal,_id)=>{
                return(
                        <Deal
                            key={_id}
                            id={_deal.id}
                            imgURL={_deal.imgURL}
                            price = {_deal.price}
                            extraImages = {_deal.extraImages}
                        />
                      );
                    })
            }
        </Grid>
        {/* BEAGIN BRAND PART */}
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Typography
                  component="h2"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.featuredbrands}
                >
                FEATURED BARANDS  
              </Typography>
          </Grid>
          <Grid item xs={12} sm = {12}>
            <Typography
                component="h2"
                variant="subtitle1"
                color="inherit"
                align="left"
                noWrap
               >
              Incredible prices you won't want to miss
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
              <Card className={classes.brandcard}> 
                <CardActionArea>
                  <CardMedia
                    className={classes.dealmedia}
                    image={barnd1}
                    title=" "
                  />
                </CardActionArea>
              </Card>
              <Typography
                  component="h6"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.flashdeals}
                >
                Shop sales now 
              </Typography>
                <Typography variant="subtitle1" color="textSecondary" className={classes.flashdealstitle}>Don't miss great items at incredible prices
              </Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
              <Card className={classes.brandcard}>
                <CardActionArea>
                  <CardMedia
                    className={classes.dealmedia}
                    image={barnd2}
                    title=" "
                  />
                </CardActionArea>
              </Card>
              <Typography
                  component="h2"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.flashdeals}
                >
                Coming soon
              </Typography>
              <Typography variant="subtitle1" color="textSecondary" className={classes.flashdealstitle}>Get coupons adn add items to you cart
              </Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
              <Card className={classes.brandcard}>
                <CardActionArea>
                  <CardMedia
                    className={classes.dealmedia}
                    image={barnd3}
                    title=" "
                  />
                </CardActionArea>
              </Card>
              <Typography
                  component="h2"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.flashdeals}
                >
                NEW BARNDS
              </Typography>
              <Typography variant="subtitle1" color="textSecondary" className={classes.flashdealstitle}>The latest amivals
              </Typography>
          </Grid>
        </Grid>
        {/* END BARND PART */}
      </Container>
    </footer>
  );}
}

ConnectedBrands.propTypes = {
  classes: PropTypes.object.isRequired
};


const Brands = connect(mapStateToProps,mapDispatchToProps)(ConnectedBrands);

export default withStyles(useStyles)(Brands);