import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';    
import TableRow from '@material-ui/core/TableRow';
import { connect } from "react-redux";
import Paper from '@material-ui/core/Paper';
import {Button} from "react-bootstrap";
import Basket from '../components/Basket';




import {showCheckOut} from '../../js/actions/Home';


const useStyles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: theme.spacing(72),
  },
});



const mapStateToProps = state => {
    // console.log("Current Quantity:"+state.Baskets.current_quantity)
    return { 
        items: state.Baskets.items,
        currentQuantity:state.Baskets.current_quantity,
        totalPrice:state.Baskets.totalPrice,
    };
  };


function mapDispatchToProps(dispatch) {
    return{
      gotoCheckOut: () => { dispatch(showCheckOut()) },
    };
}


class  ConnectedBaskets extends React.Component {
    

  showCheckOut = (e)=>{
    this.props.gotoCheckOut();
  }
    render(){
        const {classes} = this.props;
        return ( 
            <Paper className={classes.root} elevation = {0}> 
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell align="center">ProductName</TableCell>
                    <TableCell align="center">price</TableCell>
                    <TableCell align="center">Quantity</TableCell>
                    <TableCell align="right">total</TableCell>
                    <TableCell align="center">Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.props.items.map((item,i) => (  
                   <Basket 
                            key = {i}
                            _id = {i}
                            currentQuantity = {this.props.currentQuantity}
                            item = {item}
                    />
                  ))}
                  <TableRow>
                  <TableCell rowSpan={3} />
                    <TableCell colSpan={3}>Total:</TableCell>
                    {/* Total Price */}
                    <TableCell align="right">{this.props.totalPrice}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={6}>
                      <Button  variant="danger"  block onClick = {(e)=>this.showCheckOut(e)}>
                        CheckOut
                      </Button>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Paper>
          );
    }
}

ConnectedBaskets.propTypes = {
    classes: PropTypes.object.isRequired
};
const Baskets = connect(mapStateToProps,mapDispatchToProps)(ConnectedBaskets);
export default withStyles(useStyles)(Baskets);
