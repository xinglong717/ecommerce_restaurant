import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


/* Detail Image */
import featurebarnd1 from '../assets/img/products/watch/u1.jpg';
import featurebarnd11 from '../assets/img/products/watch/u2.jpg';
import featurebarnd2 from '../assets/img/detailbrands/2.jpg';
import featurebarnd3 from '../assets/img/products/it/5.jpg';
import featurebarnd4 from '../assets/img/products/it/6.jpg';
import featurebarnd5 from '../assets/img/detailbrands/1.jpg';
import featurebarnd6 from '../assets/img/products/socks/5.jpg';




const useStyles = makeStyles(theme => ({
    root: {
      marginTop:theme.spacing(2),
      flexGrow: 1,
    },
    productImg: {
      transition: "0.3s",
      boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
      "&:hover": {
        boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
      },
      padding:theme.spacing(2),
      width:theme.spacing(24),
      height: theme.spacing(24),
    },
    brandTitle:{
      flex: 1,
      fontWeight:`bold`,
      marginTop :theme.spacing(0),
    },
  }));



export default  function Product(props){
    const classes = useStyles();
    return(   
    <Container>
      <div className={classes.root}>
        <Typography
                      component="h2"
                      variant="h5"
                      color="inherit"
                      align="left"
                      noWrap
                      className={classes.brandTitle}
                    >
            FEATURED BARANDS  
        </Typography>
        <Typography variant="subtitle1" color="textSecondary">A wide selection of items for you</Typography>
      <Grid container spacing={3} 
            direction="row"
            justify="center"
            alignItems="stretch">
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd1} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd2} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd3} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd4} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd5} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd6} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={6} sm={4}>
        <Typography
                  component="h2"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.brandTitle}
                >
              Kids
        </Typography>
        </Grid>
        <Grid item xs={6} sm={4}>
        <Typography
                  component="h2"
                  variant="h6"
                  color="inherit"
                  align="left"
                  noWrap
                  className={classes.brandTitle}
                >
          Tech  
        </Typography>
        </Grid>
        <Grid item xs={6} sm={4}>
        <Typography
                    component="h2"
                    variant="h6"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.brandTitle}
                  >
                  Beauty 
                </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={3}>
      <Grid item xs={12} sm={2}>
          <img src = {featurebarnd11} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd2} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd3} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd4} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd5} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={12} sm={2}>
          <img src = {featurebarnd6} className={classes.productImg}  alt ="" />
        </Grid>
        <Grid item xs={6} sm={4}>
        <Typography
                    component="h2"
                    variant="h6"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.brandTitle}
                  >
                  Women's Fashion
                </Typography>
        </Grid>
        <Grid item xs={6} sm={4}>
        <Typography
                    component="h2"
                    variant="h6"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.brandTitle}
                  >
                  Sports  
                </Typography>
        </Grid>
        <Grid item xs={6} sm={4}>
        <Typography
                    component="h2"
                    variant="h6"
                    color="inherit"
                    align="left"
                    noWrap
                    className={classes.brandTitle}
                  >
                  Home 
                </Typography>
        </Grid>
      </Grid>
    </div>
  </Container>
);}