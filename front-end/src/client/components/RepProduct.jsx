import React from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = theme => ({
    root:{
        marginTop:theme.spacing(2),
    },
    toolbarTitle: {
        flex: 1,
        fontWeight:`bold`,
      },
    media: {
      height: theme.spacing(12),
    }
  });


class RepProduct extends React.Component{
    render(){
        const {classes} = this.props;
        return(
            <Grid item xs={3} sm={2} className = {classes.root}>
                <Typography
                            component="h2"
                            variant="subtitle1"
                            color="inherit"
                            align="center"
                            className={classes.toolbarTitle}
                        >
                        .Beauty
                </Typography>
                <Card  elevation = {0}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image={this.props.imgURL}
                            title=" "
                        />
                    </CardActionArea>
                </Card>
            </Grid>
        );
    }
}

export default withStyles(useStyles)(RepProduct);