import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import ButtonBase from '@material-ui/core/ButtonBase';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';

import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';

/**
 * Redux Actions
 */
import {showDetailPart} from '../../js/actions/Home';
import {addDetailProductImageURL} from '../../js/actions/Home';



const useStyles = theme => ({
  card: {
    transition: "0.3s",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
    },
    maxWidth: theme.spacing(28),
  },
  cardAction: {
    display: 'block',
    width:theme.spacing(28),
    textAlign: 'center'
  },
  dealmedia:{
    height: theme.spacing(24),
  }
});


function mapDispatchToProps(dispatch) {
  return {
    gotoShowDetailPart: content => dispatch(showDetailPart(content)),
    addDetailProductImageURL:content => dispatch(addDetailProductImageURL(content))
  };
}



class ConnectedDeals extends React.Component{

  componentDidMount(){
  }

  goToDetailPart = (productId) =>{
    this.props.gotoShowDetailPart(true);
    this.props.addDetailProductImageURL(productId);
  }

  render(){

    const {classes} = this.props;
    
    return(
            <Grid item xs>
              <Card className={classes.card}>
                <ButtonBase className={classes.cardAction} >
                  <CardMedia
                    className={classes.dealmedia}
                    image={this.props.imgURL}
                    title=""
                    onClick = {(e) =>this.goToDetailPart(this.props.id,e)}
                  />
                </ButtonBase>  
                <CardContent>
                  <Typography gutterBottom variant="h6" component="h2">€{this.props.price}</Typography>
                  <Typography variant="body2" color="textSecondary" component="p" align = "right">€12.45</Typography>
                </CardContent>  
              </Card>   
            </Grid>            
  );}
}

ConnectedDeals.propTypes = {
  classes: PropTypes.object.isRequired
};

const Deals = connect(null, mapDispatchToProps)(ConnectedDeals);
export default withStyles(useStyles)(Deals);