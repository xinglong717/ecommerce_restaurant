import React from 'react';
import { connect } from "react-redux";
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import ReactImageMagnify from 'react-image-magnify';
import { withStyles } from '@material-ui/core/styles';
import { Button,Image} from "react-bootstrap";
import {addDetailProductImageURL} from '../../js/actions/Home';
import {addProductInfoInBasket} from '../../js/actions/ProductDetailAction';
import {selectedColorImage} from '../../js/actions/ProductDetailAction';


import FavoriteIco from '@material-ui/icons/FavoriteBorderOutlined';

import DetailCouponImg1 from '../assets/img/detail_coupon_1.png';


import ShirtDetailRecommend0001 from '../assets/img/products/shirts/detail/0001/shirt_0001_0002_1.jpg';
import ShirtDetailRecommend0002 from '../assets/img/products/shirts/detail/0001/shirt_0001_0002_2.jpg';
import ShirtDetailRecommend0003 from '../assets/img/products/shirts/detail/0001/shirt_0001_0002_3.jpg';



const useStyles = theme =>({
    root:{
        cursor: 'pointer',
        marginTop:theme.spacing(2),
    },
    description:{
        marginBottom:theme.spacing(1),
    },
    couponImg:{
        margin:theme.spacing(1),
    },
    colorImg:{
        marginTop:theme.spacing(1),
        marginBottom:theme.spacing(1),
        marginRight:theme.spacing(1),
    },
    shoppingButton:{
        marginTop:theme.spacing(4),
        marginRight:theme.spacing(4),
    },
    hoverImg:{
        '&:hover': {
            borderColor: "#f00",
        },
        width:theme.spacing(8),
        height:theme.spacing(8)
    },
    clickImg:{
            
            borderColor: "#f00",
            width:theme.spacing(8),
            height:theme.spacing(8)
    },
    imgThumb:{
        margin:theme.spacing(2),
    }
});

function mapDispatchToProps(dispatch) {
    return {
        addDetailProductImageURL:content => dispatch(addDetailProductImageURL(content)),
        selectedColorImage:content =>dispatch(selectedColorImage(content)),
        // addProductToCart:content =>dispatch(addProductInBasket(content)),
        addProductInforToCart:content => dispatch(addProductInfoInBasket(content))   
    };
  }

const mapStateToProps = state => {
    // console.log(state.FlashDealProducts.selectedProduct[0].colorImage)
    return { 
        imgURL:state.FlashDealProducts.currentImageURL,
        product:state.FlashDealProducts.selectedProduct[0],
        price:state.FlashDealProducts.selectedProduct[0].price,
        thumbImgs:state.FlashDealProducts.selectedProduct[0].thumbImage,
        colorImgs:state.FlashDealProducts.selectedProduct[0].colorImage,
        currentProductAmountInCart:state.Baskets.amount_Product_PlacedIn_Cart,
    };
};

class ConnectedProductImageZoom extends React.Component{
   
    constructor(props){
        super(props);
        this.state = {
            indexOfClickedItem: -1,
        }
    }
    handleOverImage = (imgURL)=>{
        this.props.selectedColorImage(imgURL);
    }
    handleOnClickImage = (imgURL,id)=>{
        this.setState({indexOfClickedItem: id});
        this.props.selectedColorImage(imgURL);
    }
    addCart = () =>{
        this.props.addProductInforToCart(this.props.product);
    }
    render(){
        const {classes} = this.props;
        return(
            <Grid container spacing={3} className = {classes.root}>
                <Grid item xs={12} sm={5}>
                   <Grid item>
                        <ReactImageMagnify {...{
                            smallImage: {
                                alt: 'Wristwatch by Ted Baker London',
                                isFluidWidth: true,
                                src: this.props.imgURL 
                            },
                            largeImage: {
                                src: this.props.imgURL,
                                width: 1200,
                                height: 1800
                            },
                            enlargedImagePosition: 'over',
                        }} />
                    </Grid>
                    <Grid item>
                       <Grid container direction="row" justify="center" alignItems="center" >
                            {
                                this.props.thumbImgs.map((_thumbImg,_id)=>{
                                    return(
                                            <Grid className = {classes.colorImg} key={_id}>
                                                <Image src = {_thumbImg.src} thumbnail  className = {classes.hoverImg} onMouseOver = {(e) =>this.handleOverImage(_thumbImg.src,e)} alt ="" />
                                            </Grid>
                                        );
                                })
                            }
                        </Grid>                        
                    </Grid>
                </Grid>
                <Grid item xs={6} sm={5} >
                    <Grid item className = {classes.description}>
                        Summer shirt for men quattro luxury shirt for men car casual clothes brand car comfortable shirt high quality shirt 2019
                    </Grid>
                    <Grid item className = {classes.description}>
                        23orders
                    </Grid>
                    <Divider className = {classes.description}/>
                    <Grid item className = {classes.description}>
                        <Grid container alignItems="flex-end">
                            <Grid className = {classes.colorImg}>
                                <Typography
                                    component="h2"
                                    variant="h5"
                                    color="inherit"
                                    align="left"
                                    noWrap                        
                                >
                                 €{this.props.price}
                                </Typography>
                            </Grid>
                            <Grid className = {classes.colorImg}>
                                <Typography
                                    component="h2"
                                    variant="subtitle1"
                                    color="inherit"
                                    align="right"
                                    noWrap                        
                                >
                                    €16.15 - 22.67
                                </Typography>
                            </Grid>
                        </Grid>                   
                    </Grid>
                    <Grid item className = {classes.description}>
                            <Grid container>
                                <Grid className = {classes.couponImg}>
                                    <img src = {DetailCouponImg1}  alt ="" />
                                </Grid>
                                <Grid className = {classes.couponImg}>
                                    <img src = {DetailCouponImg1} alt ="" />
                                </Grid>
                            </Grid> 
                    </Grid>
                    <Divider />
                    <Grid item>
                        Color:
                    </Grid>
                    <Grid item>
                        <Grid container >
                            {
                                this.props.colorImgs.map((_colorImg,_id)=>{
                                    return(
                                            <Grid className = {classes.colorImg} key={_id}>
                                                <Image src = {_colorImg.src} thumbnail id= {_id}  className = {this.state.indexOfClickedItem === _id? classes.clickImg:classes.hoverImg} onClick = {(e) =>this.handleOnClickImage(_colorImg.src,_id,e)}  alt ="" />
                                            </Grid>
                                            
                                        );
                                })
                            }
                        </Grid>
                    </Grid>
                    <Grid item >
                        <Grid container>
                            <Grid className = {classes.shoppingButton}>
                                <Button variant="danger" >&nbsp;&nbsp;&nbsp;Buy Now&nbsp;&nbsp;&nbsp;</Button>
                            </Grid>
                            <Grid className = {classes.shoppingButton}>
                                <Button variant="warning"  onClick = {this.addCart}>&nbsp;&nbsp;Add to Cart&nbsp;&nbsp;</Button>
                            </Grid>
                            <Grid className = {classes.shoppingButton}>
                                <Button variant="outline-secondary"><FavoriteIco/>&nbsp;38&nbsp;</Button>
                            </Grid>
                        </Grid>
                    </Grid>
            </Grid>
            <Grid item xs={6} sm={2}>
                <Typography
                    component="h2"
                    variant="h6"
                    color="inherit"
                    align="center"
                                           
                >
                Recommended For You
                </Typography>
                <Grid item >
                    <Image src = {ShirtDetailRecommend0001} thumbnail  alt ="" />
                </Grid>
                <Grid item >
                    €8.72
                </Grid>
                <Grid item >
                    <Image src = {ShirtDetailRecommend0002} thumbnail  alt ="" />
                </Grid>
                <Grid item >
                    €8.72
                </Grid>
                <Grid item >
                    <Image src = {ShirtDetailRecommend0003} thumbnail  alt ="" />
                </Grid>
                <Grid item >
                    €8.72
                </Grid>
            </Grid>
        </Grid>
        );
    }
}


ConnectedProductImageZoom.propTypes = {
  classes: PropTypes.object.isRequired
};
const ProductImageZoom = connect(mapStateToProps,mapDispatchToProps)(ConnectedProductImageZoom);
export default withStyles(useStyles)(ProductImageZoom);