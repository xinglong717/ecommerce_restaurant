import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import { Image} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import FavoriteOutlined from '@material-ui/icons/FavoriteBorderOutlined';

import {deleteProductInBasket} from '../../js/actions/BasketsAction';
import {incrementTotalPrice} from '../../js/actions/BasketsAction';
import {decrementQuantity} from '../../js/actions/BasketsAction';
import {incrementQuantity} from '../../js/actions/BasketsAction';
import {decrementTotalPrice} from '../../js/actions/BasketsAction';



    const useStyles = theme => ({
        quantityNumber:{
            marginLeft:theme.spacing(4),
            marginRight:theme.spacing(4)
        },
        thumbImg:{
            width:theme.spacing(12),
            height:theme.spacing(12)
        }
    });


    function mapDispatchToProps(dispatch) {
        return {
            gotoDeleteProductInBasket: content => dispatch(deleteProductInBasket(content)),
            incrementTotalPrice:content => dispatch(incrementTotalPrice(content)),
            incrementQuantity:content => dispatch(incrementQuantity(content)),
            decrementQuantity:content => dispatch(decrementQuantity(content)),
            decrementTotalPrice:content => dispatch(decrementTotalPrice(content)),
        };
    }


class  ConnectedBasket extends React.Component {
   
    // componentDidMount(){
    //     console.log(this.props.currentQuantity)
    // }

    increment = (currentId) => {
        this.props.incrementTotalPrice(this.props.item.price);
        this.props.incrementQuantity(currentId);
    }

    decrement = (currentId) => {
        // console.log(this.props.currentQuantity)
        var tempQuantity = this.props.currentQuantity - 1;
        if( tempQuantity < 0){
            tempQuantity = 0;  
        }else{ 
            this.props.decrementQuantity(currentId);
            this.props.decrementTotalPrice(this.props.item.price);
        }
    }

    deleteItem(i) {
        // console.log(this.props.item.currentQuantity)
        // console.log(this.props.item.price)
        this.props.decrementTotalPrice(this.props.item.price*this.props.item.currentQuantity);
        this.props.gotoDeleteProductInBasket(i);
    }
    render(){
        const {classes} = this.props;
        return (  
                   <TableRow>
                      <TableCell> <Image src = {this.props.item.imgURL} thumbnail className = {classes.thumbImg}  alt ="" /></TableCell>
                      <TableCell align="center">{this.props.item.name}</TableCell>
                      <TableCell align="center">{this.props.item.price}</TableCell>
                      <TableCell align="center">
                        <div>
                            <button onClick={(e)=>this.decrement(this.props.item.id,e)}>-</button>
                            <span className = {classes.quantityNumber}>{this.props.item.currentQuantity}</span>
                            <button onClick={(e)=>this.increment(this.props.item.id,e)}>+</button>
                        </div>
                        </TableCell>
                        {/* Sub Total price */}
                        <TableCell align="right">{this.props.item.price * this.props.item.currentQuantity}</TableCell>
                        <TableCell align="center">
                            <Button
                                onClick={this.deleteItem.bind(this,this.props.item.id)}
                                color="default"
                            >
                                <DeleteForeverOutlinedIcon/>
                            </Button>
                            <Button
                                color="secondary"
                            >
                                <FavoriteOutlined/>
                            </Button>
                        </TableCell>
                    </TableRow>
          );
    }
}

ConnectedBasket.propTypes = {
    classes: PropTypes.object.isRequired
};
const Basket = connect(null, mapDispatchToProps)(ConnectedBasket);
export default withStyles(useStyles)(Basket);
