import React from 'react';
import { InputGroup,Button,DropdownButton,Dropdown,FormControl} from "react-bootstrap";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';



import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';

import "react-responsive-carousel/lib/styles/carousel.min.css";
import 'bootstrap/dist/css/bootstrap.min.css';

/* Icon Group */
import mark from './assets/img/dysoft.png';
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

import SearchIcon from '@material-ui/icons/Search';
import FavoriteOutlined from '@material-ui/icons/FavoriteBorderOutlined';
import PermIdentity from '@material-ui/icons/PermIdentityOutlined';

import ProductDetails from './views/ProductDetails';
import MainPart from './views/Main';
import Brands from './views/Brands';
import Products from './views/Products';
import MainServices from './views/MainServices';
import ExtraServices from './views/ExtraServices';
import ShowBaskets from './views/Baskets';
import CheckOut from './views/Checkout';
import ProductList from './views/ProductsList';

import {showHome} from '../js/actions/Home';
import {showBaskets} from '../js/actions/Home';

function HideOnScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={trigger}>
      {children}
    </Slide>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.node.isRequired,
  // Injected by the documentation to work in an iframe.
  // You won't need it on your project.
  window: PropTypes.func,
};


  
const useStyles = theme => ({
  root: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(8),
    padding: theme.spacing(6, 0),
  },
  icon: {
    marginLeft: theme.spacing(4),
    cursor: 'pointer',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  markimage: {
    cursor: 'pointer',
  },
  searchInput:{
    fontSize:12,
  },
  toolbar: {
    marginTop :theme.spacing(8),
    height:theme.spacing(12), 
    borderBottom: `1px solid ${theme.palette.divider}`,
    backgroundColor:`#ffffff`,
  },
  badge: {
    top: '50%',
  } 
});

const mapStateToProps = state => {
  // console.log(state.Home.showCheckOut)
  return { 
          showMainPart: state.Home.showMainPart,
          showBrand: state.Home.showBrand,
          showProduct: state.Home.showProduct,
          showService: state.Home.showService,
          showExtraService: state.Home.showExtraService,
          showProductsDetails:state.Home.showProductsDetails,
          showBaskets:state.Home.showBaskets,
          showCheckOut:state.Home.showCheckOut,
          showProductList:state.Home.showProductList,
          amount_Product_PlacedIn_Cart:state.Baskets.amount_Product_PlacedIn_Cart,
        };
};


function mapDispatchToProps(dispatch) {
  return{
    gotoShowHome: () => { dispatch(showHome()) },
    gotoShowBaskets:() => { dispatch(showBaskets()) },
  };
}

class ConnectedHome extends React.Component{
  
    state = {
      searchListCategorys:[
                            {
                              sText:"Men's fashion",
                              id:"Men's fashion",
                            },
                            {
                              sText:"Women's Clothing",
                              id:"Women's Clothing",
                            },
                            {
                              sText:"Telephony",
                              id:"Telephony",
                            },
                            {
                              sText:"Computing",
                              id:"Computing",
                            },
                            {
                              sText:"electronics",
                              id:"electronics",
                            },
                            {
                              sText:"Jewelry and Watches",
                              id:"Jewelry and Watches",
                            },
                            {
                              sText:"House and Garden",
                              id:"House and Garden",
                            },
                            {
                              sText:"Bags and Footwear",
                              id:"Bags and Footwear"
                            },
                            {
                              sText:"Sports and Leisure",
                              id:"Sports and Leisure"
                            },
                            {
                              sText:"Health and Beauty",
                              id:"Health and Beauty"
                            }
                          ],
    };
  
    markImageOnClick  = () =>{
      this.props.gotoShowHome();
    }

    basketOnClick = () =>{
      this.props.gotoShowBaskets();
    }
  render(){
    const {classes} = this.props;
    
    return (
      <React.Fragment>
        {/* START APPBAR */}
        <HideOnScroll {...this.props}>
          <AppBar color = "inherit">
            <Container maxWidth = "lg">
              <Toolbar>
                <Grid container>
                  <Grid item xs={12} md = {1}>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="Menu">
                      <MenuIcon />
                    </IconButton>
                  </Grid>
                  <Grid item xs = {12} md = {1}>
                    <Typography variant="h6">Dy-Soft</Typography>
                  </Grid>
                  <Grid item xs ={12} md = {10}>
                    <InputGroup>
                       <FormControl
                         placeholder="Seller Centers"
                         aria-label="Recipient's username"
                         aria-describedby="basic-addon2"
                        />
                        <InputGroup.Append>
                          <Button variant="danger"><SearchIcon /></Button>
                        </InputGroup.Append>
                    </InputGroup>
                  </Grid>
                </Grid>        
              </Toolbar>
            </Container>
          </AppBar>
        </HideOnScroll>
        {/* End AppBar */}
  
        <Toolbar className={classes.toolbar}>
            <Container >
                <Grid container spacing={3}
                  direction="row"
                  justify="center"
                  alignItems="flex-end">
                  <Grid item xs={12} md={3}>
                    <img src={mark} className = {classes.markimage} onClick={this.markImageOnClick} alt =""/>
                  </Grid>
                  <Grid item xs={12} md={6}>               
                      <InputGroup>
                        <FormControl
                          placeholder="Seller Centers"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                        />
                        <DropdownButton
                          as={InputGroup.Append}
                          variant="outline-secondary"
                          title="All Categories"
                          id="input-group-dropdown-2"
                          
                        > 
                          {
                            this.state.searchListCategorys.map((category)=>{
                              return(
                                <Dropdown.Item href="#" key = {category.id} className = {classes.searchInput}>{category.sText}</Dropdown.Item>
                              );
                            })
                          }
                        </DropdownButton>
                        <InputGroup.Append>
                          <Button variant="danger"><SearchIcon /></Button>
                        </InputGroup.Append>
                      </InputGroup>
                  </Grid>
                  <Grid item xs={12} md={3}>
                  <Badge className={classes.badge} badgeContent={this.props.amount_Product_PlacedIn_Cart} color = "primary">
                    <ShoppingCartIcon className={classes.icon} color="secondary" onClick = { this.basketOnClick } />
                  </Badge>
                  <FavoriteOutlined className={classes.icon} color = "secondary"/>
                  <PermIdentity className ={classes.icon} color = "secondary" />
                  </Grid>
                </Grid>
            </Container>
        </Toolbar>
        <Container maxWidth = "lg" > 
          { this.props.showProductsDetails ? <ProductDetails /> : null }
          { this.props.showCheckOut ?<CheckOut/>: null}
          { this.props.showBaskets ?<ShowBaskets/>:null} 
          { this.props.showProductList? <ProductList/>:null}
          {/* BEAGIN MAIN PART */}
          { this.props.showMainPart? <MainPart/>:null }
          {/* END MAIN PART */}
        </Container>
       
        {/* BEGIN DETAIL PART */}
          {this.props.showBrand ?<Brands/>:null}
        {/* END DETAIL PART */}
          {this.props.showProduct ?<Products/>:null}
        {/* START JUST FOR YOU */}
          {this.props.showService ?<MainServices/>:null}
        {/* END JUST FOR YOU */}
        {/* START FOOTER */}
       
          {this.props.showExtraService ?<ExtraServices/>:null}
        {/* <footer className={classes.footer}>
          <Container maxWidth="lg">
          </Container>
        </footer> */}
        {/* END FOOTER */}
      </React.Fragment>
    );
  }
}

ConnectedHome.propTypes = {
  classes: PropTypes.object.isRequired
};
const Home = connect(mapStateToProps,mapDispatchToProps)(ConnectedHome);
export default withStyles(useStyles)(Home)