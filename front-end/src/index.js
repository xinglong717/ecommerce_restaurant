import React from 'react';
import { Router, Route, Switch } from "react-router-dom";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from "./js/store/index";
import { createBrowserHistory } from "history";
import Home from './client/Home';
import Dashboard from './client/views/ProductDetails';

var hist = createBrowserHistory();
ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/admin" component={Dashboard} />
      </Switch>
    </Router>   
  </Provider>,
    document.getElementById("root")
  );
  

