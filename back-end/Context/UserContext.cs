﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using rest.Model;

namespace rest.Context
{
    public class UserContext
    {
        private readonly IMongoDatabase _database = null;
        public UserContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<User> Users
        {
            get
            {
                return _database.GetCollection<User>("User");
            }
        }
    }
}
