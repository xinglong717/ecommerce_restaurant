using Microsoft.Extensions.Options;
using MongoDB.Driver;
using rest.Model;

namespace rest.Context
{
    public class ProductContext{

    private readonly IMongoDatabase _database = null;

        public ProductContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Product> Products
        {
            get
            {
                return _database.GetCollection<Product>("Product");
            }
        }
    }
}