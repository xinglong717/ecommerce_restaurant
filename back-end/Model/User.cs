
using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace rest.Model
{
    public class User
    {
        [BsonId]
        public ObjectId Id { get; set; }    
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string eMail { get; set; }
        public string Password { get; set; }
    }
}