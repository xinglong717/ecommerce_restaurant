﻿using Microsoft.Extensions.Options;
using rest.Model;
using rest.Repository.Interface;
using rest.Context;
using System.Threading.Tasks;

namespace rest.Repository.Interface
{
    public class UserRepository:IUserRepository
    {
        private readonly UserContext _context = null;

        public UserRepository()
        {
        }

        public UserRepository(IOptions<Settings> settings)
        {
            _context = new UserContext(settings);
        }

        public async Task AddUser(User item)
        {
            await _context.Users.InsertOneAsync(item);
        }
    }    
}
