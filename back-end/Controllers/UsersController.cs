﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using rest.Model;
using rest.Repository.Interface;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace rest.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {

        private readonly IUserRepository _UserRepository;

        public UsersController(IUserRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }

        // POST api/Products
        [HttpPost]
        public void Post([FromBody]User user)
        {
            _UserRepository.AddUser(new User()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                eMail = user.eMail,
                Password = user.Password
            });
        }
    }
}
